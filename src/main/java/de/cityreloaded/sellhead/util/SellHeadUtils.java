package de.cityreloaded.sellhead.util;

import de.cityreloaded.sellhead.SellHead;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Lars on 13.11.2016.
 */
public class SellHeadUtils {
  private static final double HEAD_PRICE = 0.2D;

  public static double sellHeads(Player player, ItemStack[] itemStacks) {
    double price = 0;

    for (ItemStack itemStack : itemStacks) {
      if (itemStack != null && itemStack.getType() == Material.SKULL_ITEM) {
        price += HEAD_PRICE * itemStack.getAmount();
      }
    }

    if (price > 0) {
      for (ItemStack itemStack : itemStacks) {
        int firstSlot = player.getInventory().first(itemStack);

        if (firstSlot != -1) {
          player.getInventory().setItem(firstSlot, new ItemStack(Material.AIR, 1));
        } else {
          // itemstack not here
        }
      }

      SellHead.getInstance().getVaultApi().addMoney(player, price);
    }

    return price;
  }

}
