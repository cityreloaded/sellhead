package de.cityreloaded.sellhead;

import de.cityreloaded.sellhead.commands.AllCommand;
import de.cityreloaded.sellhead.commands.HelpCommand;
import de.cityreloaded.sellhead.commands.SellCommand;
import de.paxii.bukkit.commandapi.CommandApi;
import de.paxii.bukkit.vaultapi.VaultApi;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;

/**
 * Created by Lars on 13.11.2016.
 */
public class SellHead extends JavaPlugin {

  @Getter
  private static SellHead instance;

  @Getter
  private CommandApi commandApi;

  @Getter
  private VaultApi vaultApi;

  @Override
  public void onEnable() {
    SellHead.instance = this;
    this.commandApi = new CommandApi();
    this.vaultApi = new VaultApi();
    this.commandApi.setChatPrefix(ChatColor.GOLD + "[SellHead] " + ChatColor.RESET);

    if (!this.vaultApi.setupEconomy()) {
      System.out.println(this.commandApi.getChatPrefix() + "Could not register with Vault");
      this.setEnabled(false);
    }

    this.commandApi.addCommand(new SellCommand());
    this.commandApi.addCommand(new AllCommand());
    this.commandApi.addCommand(new HelpCommand());

    this.getCommand("sellhead").setExecutor(this.commandApi.getExecutor());
  }

}
