package de.cityreloaded.sellhead.commands;

import de.cityreloaded.sellhead.util.SellHeadUtils;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Created by Lars on 13.11.2016.
 */
public class AllCommand extends AbstractCommand {
  @Override
  public String getCommand() {
    return "all";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"sellhead"};
  }

  @Override
  public String getHelp() {
    return "Verkauft alle Köpfe in deiner Hotbar.";
  }

  @Override
  public String getPermission() {
    return "SellHead.All";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());
    ArrayList<ItemStack> itemStacks = new ArrayList<>();

    for (int slot = 0; slot <= 8; slot++) {
      ItemStack itemStack = player.getInventory().getItem(slot);
      if (itemStack != null && (itemStack.getType() == Material.SKULL_ITEM || itemStack.getType() == Material.SKULL)) {
        itemStacks.add(itemStack);
      }
    }

    double price = SellHeadUtils.sellHeads(player, itemStacks.toArray(new ItemStack[itemStacks.size()]));

    if (price > 0) {
      this.sendMessage(commandSender, String.format("Dir wurden $%.2f gutgeschrieben.", price));
    } else {
      this.sendMessage(commandSender, "Du hast keinen Kopf in der Hotbar.");
    }

    return true;
  }
}
