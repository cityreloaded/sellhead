package de.cityreloaded.sellhead.commands;

import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Lars on 13.11.2016.
 */
public class HelpCommand extends AbstractCommand {

  private static final String[] messages = new String[]{
          "&6&l[]=========================================[]",
          "&b&lKöpfe verkaufen",
          "&6So funktioniert das Köpfe verkaufen:",
          "&61. Kopf, der verkauft werden soll, in die Hand nehmen.",
          "&62. &2/sellhead sell &6eingeben",
          "&6Nun wird der Kopf in deiner Hand für &a$&20.2 &6pro Kopf verkauft.",
          "&6Gestackte Köpfe können auch verkauft werden.",
          "&6Funktioniert mit Spieler und Mob Köpfen.",
          "&6Um alle Köpfe, die aktuell in der Hotbar sind, auf einmal zu verkaufen,",
          "&6gib &2/sellhead all &6ein.",
          "&6&l[]=========================================[]"
  };

  @Override
  public String getCommand() {
    return "help";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"sellhead"};
  }

  @Override
  public String[] getAliases() {
    return new String[]{"hilfe"};
  }

  @Override
  public String getHelp() {
    return "";
  }

  @Override
  public String getPermission() {
    return "SellHead.Help";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    for (String message : HelpCommand.messages) {
      commandSender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    return true;
  }
}
