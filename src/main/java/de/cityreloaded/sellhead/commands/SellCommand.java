package de.cityreloaded.sellhead.commands;

import de.cityreloaded.sellhead.util.SellHeadUtils;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Lars on 13.11.2016.
 */
public class SellCommand extends AbstractCommand {
  @Override
  public String getCommand() {
    return "sell";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"sellhead"};
  }

  @Override
  public String getHelp() {
    return "Verkauft die Köpfe in deiner Hand.";
  }

  @Override
  public String getPermission() {
    return "SellHead.Sell";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());
    ItemStack itemStack = player.getItemInHand();
    double price = SellHeadUtils.sellHeads(player, new ItemStack[]{itemStack});

    if (price > 0) {
      this.sendMessage(commandSender, String.format("Dir wurden $%.2f gutgeschrieben.", price));
    } else {
      this.sendMessage(commandSender, "Du hast keinen Kopf in der Hand.");
    }

    return true;
  }
}
